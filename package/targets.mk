# SPDX-License-Identifier: ISC

BIN_TARGETS=		sz-chroot			\
			sz-download			\
			sz-error			\
			sz-mdfilter			\
			sz-mincss			\
			sz-tmpfs

BIN_TARGETS+=		sz-jail-cache			\
			sz-jail-down			\
			sz-jail-up

LIBEXEC_TARGETS=	sz-jail-config			\
			sz-jail-dev			\
			sz-jail-passwd			\
			sz-jail-pkgsrc			\
			sz-jail-size

SHARE_TARGETS=		OpenBSD/7.2/amd64/SHA256	\
			OpenBSD/7.2/amd64/base		\
			OpenBSD/7.2/amd64/pkgsrc

SHARE_TARGETS+=		Linux/6.0/x86_64/SHA256		\
			Linux/6.0/x86_64/base		\
			Linux/6.0/x86_64/pkgsrc
